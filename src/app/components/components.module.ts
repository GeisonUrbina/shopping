import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SlideMenuComponent } from './slide-menu/slide-menu.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SecondaryHeaderComponent } from './secondary-header/secondary-header.component';



@NgModule({
  declarations: [
    HeaderComponent,
    SlideMenuComponent,
    SecondaryHeaderComponent
  ],
  exports:[
    HeaderComponent,
    SlideMenuComponent,
    SecondaryHeaderComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ]
})
export class ComponentsModule { }
