import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { LinkList,User } from '../../interfaces/interfaces';
import { Observable } from 'rxjs';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-slide-menu',
  templateUrl: './slide-menu.component.html',
  styleUrls: ['./slide-menu.component.scss'],
})
export class SlideMenuComponent implements OnInit {
  linkList: Observable<LinkList[]>;
  constructor(private dataService:DataService,public localDataService:LocalDataService) { }

  ngOnInit() {
    this.linkList = this.dataService.getSlideMenuOptions();
  }

}
