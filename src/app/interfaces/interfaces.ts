export interface LinkList{
    icon:string;
    name:string;
    redirectTo:string;
}

export interface QuestionList{
    question:string;
    answer:string;
}

export interface CategoryList{
    category_code:string;
    category_name:string;
    category_status:string;
    category_image:string;
}

export interface SubcategoryList{
    subcategory_code:string;
    subcategory_name:string;
    subcategory_status:string;
    subcategory_image:string;
}

export interface ProductList{
    product_code:string;
    product_name:string;
    product_description:string;
    product_cost:string;
    product_com_profit:string;
    product_quantity:number;
    product_status:string;
    product_image:string;
    made_in:string;
    is_offer:string;
    monto:string;
    montoAporte:string;
}

export interface Login{
    status_response:string;
    user:User;
}
export interface User{
    id:string;
    email:string;
    client_name:string;
    client_lastName:string;
    client_role:string;
    client_phone:string;
    facebook_id:string;
    player_id:string;
    phone_code:string;
    points:string;
    phone_validated:Int16Array;
    allow_notifications:boolean;
    validation_method:string;
    password:string;
    client_status:string;
}

export interface Cart{
    cliente:string,
    codigo:string,
    cantidad:string
}

export interface Favourite{
    cliente:string;
    producto:ProductList
}

export interface Receipt{
    montoTotal:string;
    costoEnvio:string;
    montoAporteTotal:string;
    puntos:string;
    productos:ProductList[];
}

export interface Project{
    id: string;
    project_code: string;
    project_name: string;
    project_description: string;
    project_cost: number;
    project_collected_amount: number;
    project_status: string;
    project_image: string;
    warehouse_code: string;
    affiliated: boolean;
    created_at: string;
    updated_at: string;
}
