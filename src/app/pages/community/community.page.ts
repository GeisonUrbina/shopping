import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { QuestionList } from '../../interfaces/interfaces';

@Component({
  selector: 'app-community',
  templateUrl: './community.page.html',
  styleUrls: ['./community.page.scss'],
})
export class CommunityPage implements OnInit {

  questionList: Observable<QuestionList[]>;
  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.questionList = this.dataService.getFaqCommunity();
  }

}
