import { Component, OnInit } from '@angular/core';
import { LinkList, CategoryList, SubcategoryList, ProductList } from '../../interfaces/interfaces';
import { DataService } from '../../services/data.service';
import { LocalDataService } from '../../services/local-data.service';
import { Observable } from 'rxjs';
import { ApiService } from '../../services/api.service';
import { ModalController } from '@ionic/angular';
import { ProductDetailPage } from '../product-detail/product-detail.page';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {
  categoryList: CategoryList[] = [];
  subcategoryList: SubcategoryList[] = [];
  productList: ProductList[] = [];
  constructor(public dataService:DataService,public localDataService:LocalDataService,
              private apiService:ApiService,public modalCtrl: ModalController) { }

  ngOnInit() {
    this.apiService.getCategories().subscribe(resp => {
        this.categoryList.push(...resp);
    });
  }

  getSubcategories(categoryCode:string){
    this.productList = [];
    this.subcategoryList = [];
    this.apiService.getSubcategories(categoryCode).subscribe(resp=>{
      this.subcategoryList.push(...resp);
    });
  }

  getProducts(subcategoryCode:string){
    this.productList = [];
    this.apiService.getProducts(subcategoryCode).subscribe(resp=>{
      this.productList.push(...resp);
    });
  }

  searchProducts(keyWord){
    this.productList = [];
    this.apiService.searchProducts(keyWord.value).subscribe(resp => {
      this.productList.push(...resp);
    });
  }

  async presentModal(product:ProductList) {
    const modal = await this.modalCtrl.create({
      component: ProductDetailPage,
      componentProps: {
        'product': product
      }
    });
    return await modal.present();
  }
}
