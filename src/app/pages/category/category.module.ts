import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoryPageRoutingModule } from './category-routing.module';

import { CategoryPage } from './category.page';
import { ComponentsModule } from '../../components/components.module';
import { ProductDetailPage } from '../product-detail/product-detail.page';
import { ProductDetailPageModule } from '../product-detail/product-detail.module';

@NgModule({
  entryComponents:[
    ProductDetailPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoryPageRoutingModule,
    ComponentsModule,
    ProductDetailPageModule
  ],
  declarations: [CategoryPage]
})
export class CategoryPageModule {}
