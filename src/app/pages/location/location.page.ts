import { Component, OnInit, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  Marker
} from '@ionic-native/google-maps';

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {

  map: GoogleMap;
  latitud:any;
  longitud:any;
  isMapLoaded:boolean;

  constructor(private geolocation:Geolocation) { }

  ngOnInit() {
    this.loadMap();
  }

  async loadMap(){
    const location = await this.geolocation.getCurrentPosition();
    this.latitud = location.coords.latitude;
    this.longitud = location.coords.longitude;

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: this.latitud,
           lng: this.longitud
         },
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    let marker: Marker = this.map.addMarkerSync({
      title: 'Lugar de entrega',
      icon: '#00838f',
      animation: 'DROP',
      draggable:true,
      position: {
        lat: this.latitud,
        lng: this.longitud
      }
    });

    marker.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe(()=>{
      let markerlatlong = marker.getPosition();
      this.latitud = markerlatlong.lat;
      this.longitud = markerlatlong.lng;
    });

  }

}
