import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigningUpPage } from './signing-up.page';

const routes: Routes = [
  {
    path: '',
    component: SigningUpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SigningUpPageRoutingModule {}
