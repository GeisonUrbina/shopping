import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SigningUpPageRoutingModule } from './signing-up-routing.module';

import { SigningUpPage } from './signing-up.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SigningUpPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SigningUpPage]
})
export class SigningUpPageModule {}
