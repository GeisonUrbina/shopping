import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-signing-up',
  templateUrl: './signing-up.page.html',
  styleUrls: ['./signing-up.page.scss'],
})
export class SigningUpPage implements OnInit {

  constructor(public menuCtrl:MenuController) {
    this.menuCtrl.enable(false,"mainMenu");
   }

  ngOnInit() {
  }

}
