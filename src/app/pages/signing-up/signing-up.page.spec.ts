import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SigningUpPage } from './signing-up.page';

describe('SigningUpPage', () => {
  let component: SigningUpPage;
  let fixture: ComponentFixture<SigningUpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigningUpPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SigningUpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
