import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { LinkList } from '../../interfaces/interfaces';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  linkList: Observable<LinkList[]>;
  optionSlide = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };
  constructor(public menuCtrl:MenuController,private dataService:DataService) {
    this.menuCtrl.enable(true,"mainMenu");
    
   }

  ngOnInit() {
    this.linkList = this.dataService.getMainMenuOptions();
  }

}
