import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Project } from 'src/app/interfaces/interfaces';
import { ApiService } from '../../services/api.service';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.page.html',
  styleUrls: ['./project-detail.page.scss'],
})
export class ProjectDetailPage implements OnInit {
  project:Project = <Project>{};
  constructor(private router:Router,private apiService:ApiService,private localDataService:LocalDataService) { 
    if(this.router.getCurrentNavigation().extras.state){
      this.project = this.router.getCurrentNavigation().extras.state.project;
    }
  }

  ngOnInit() {
  }
  
}
