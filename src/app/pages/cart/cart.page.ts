import { Component, OnInit } from '@angular/core';
import { LocalDataService } from '../../services/local-data.service';
import { ApiService } from '../../services/api.service';
import { ProductList } from '../../interfaces/interfaces';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  slideOpts= {
    allowSlidePrev:false,
    allowSlideNext:false
  }
  productList:ProductList[]=[];
  constructor(public localDataService:LocalDataService,private apiService:ApiService) { }

  ngOnInit() {
    this.loadCart();
  }

  loadCart(){
    this.apiService.getCart(this.localDataService.user.id,this.localDataService.cart).subscribe(resp=>{
      this.productList = resp;
    });
  }

  add(productCode){
    for(let product of this.productList){
      if(productCode === product.product_code){
        product.product_quantity +=1;
        this.localDataService.modifyProductCartQuantity(this.localDataService.user.id,productCode,product.product_quantity);
      }
    }
  }
  
  deleteProduct(clientId,productCode){
    this.localDataService.deleteProductInsideCart(clientId,productCode);
    this.loadCart();
  }

  subtract(productCode:any){
    for(let product of this.productList){
      if(productCode === product.product_code){
        if(product.product_quantity > 1) {
          product.product_quantity -=1;
          this.localDataService.modifyProductCartQuantity(this.localDataService.user.id,productCode,product.product_quantity);
        }
      }
    }
  }

}
