import { Component, OnInit } from '@angular/core';
import { LocalDataService } from '../../services/local-data.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(public localDataService:LocalDataService,private router:Router) { }

  ngOnInit() {
  }

  change(type:string,title:string){
    let navigationExtras : NavigationExtras = {
      state:{
        type: type,
        title:title
      }
  }

    this.router.navigate(['generic-modification'],navigationExtras);
  }

}
