import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvitationCodePageRoutingModule } from './invitation-code-routing.module';

import { InvitationCodePage } from './invitation-code.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvitationCodePageRoutingModule,
    ComponentsModule
  ],
  declarations: [InvitationCodePage]
})
export class InvitationCodePageModule {}
