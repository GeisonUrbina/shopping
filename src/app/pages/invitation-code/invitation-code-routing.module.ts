import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvitationCodePage } from './invitation-code.page';

const routes: Routes = [
  {
    path: '',
    component: InvitationCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvitationCodePageRoutingModule {}
