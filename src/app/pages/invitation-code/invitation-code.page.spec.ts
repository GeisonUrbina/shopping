import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InvitationCodePage } from './invitation-code.page';

describe('InvitationCodePage', () => {
  let component: InvitationCodePage;
  let fixture: ComponentFixture<InvitationCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationCodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InvitationCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
