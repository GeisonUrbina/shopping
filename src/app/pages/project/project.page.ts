import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Project } from '../../interfaces/interfaces';
import { LocalDataService } from '../../services/local-data.service';
import { Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-project',
  templateUrl: './project.page.html',
  styleUrls: ['./project.page.scss'],
})
export class ProjectPage implements OnInit {

  projects: Project[] = [];
  constructor(private apiService:ApiService,public localDataService:LocalDataService,
    private router:Router) { }

  ngOnInit() {
    this.apiService.getProjects().subscribe(resp=>{
      this.projects = resp;
      for(let project of this.projects){
        this.apiService.isAffiliated(this.localDataService.user.id,project.project_code).subscribe(resp =>{
          (resp.status_response === 'AFILIADO')? project.affiliated = true: project.affiliated = false;
        });
      }
    });
  }

  detail(project){
    let navigationExtras : NavigationExtras = {
      state:{
        project: project
      }
    }
    this.router.navigate(['project-detail'],navigationExtras);
  }

  enroll(project){
    this.apiService.affiliate(this.localDataService.user.id,project.project_code).subscribe(() => {
      for(let pro of this.projects){
        pro.affiliated = false;
      }
      project.affiliated = true;
    });
  }

  unsubscribe(project){
    this.apiService.disaffiliate(this.localDataService.user.id).subscribe(() => {
      project.affiliated = false;
    });
  }



}
