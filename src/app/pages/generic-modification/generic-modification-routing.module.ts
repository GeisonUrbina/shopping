import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenericModificationPage } from './generic-modification.page';

const routes: Routes = [
  {
    path: '',
    component: GenericModificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenericModificationPageRoutingModule {}
