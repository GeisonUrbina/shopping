import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenericModificationPageRoutingModule } from './generic-modification-routing.module';

import { GenericModificationPage } from './generic-modification.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    GenericModificationPageRoutingModule,
    ComponentsModule
  ],
  declarations: [GenericModificationPage]
})
export class GenericModificationPageModule {}
