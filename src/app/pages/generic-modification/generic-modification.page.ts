import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { LocalDataService } from 'src/app/services/local-data.service';
import { FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-generic-modification',
  templateUrl: './generic-modification.page.html',
  styleUrls: ['./generic-modification.page.scss'],
})
export class GenericModificationPage implements OnInit {

  type:string;
  title:string;
  configurationForm = this.formBuilder.group({});

  constructor(private router:Router,public localDataService:LocalDataService,private formBuilder:FormBuilder,
    private apiService:ApiService) {
    this.type = this.router.getCurrentNavigation().extras.state.type;
    this.title = this.router.getCurrentNavigation().extras.state.title;
    switch(this.type){
      case 'N':
          this.configurationForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            lastName: ['', [Validators.required]]
          });
        break;
      case 'P':
          this.configurationForm = this.formBuilder.group({
            phone: ['', [Validators.required,Validators.minLength(8),Validators.maxLength(8),Validators.pattern('[0-9]+')]]
          });
        break;
      case 'E':
          this.configurationForm = this.formBuilder.group({
            email: ['', [Validators.required,Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]]
          });
        break;
    }
   }

  ngOnInit() {
  }

  modify(form: any):void{
    if(this.configurationForm.valid){
      switch(this.type){
        case 'N':
            this.apiService.updateName(this.localDataService.user.id,form.name).subscribe(resp =>{
              if(resp.status_response === 'SUCCESS'){
                this.localDataService.user.client_name = form.name;
                this.localDataService.saveUserData(this.localDataService.user);
              }
            });
            this.apiService.updateLastName(this.localDataService.user.id,form.lastName).subscribe(resp => {
              if(resp.status_response === 'SUCCESS'){
                this.localDataService.user.client_lastName = form.lastName;
                this.localDataService.saveUserData(this.localDataService.user);
              }
            });
          break;
        case 'P':
            let userTemp = <any>this.localDataService.user;
            userTemp.temp_phone = form.phone;
            let navigationExtras : NavigationExtras = {
              state : {
                status: 'PHONE_MODIFY',
                user: userTemp
              }
            }
            this.router.navigate(['/smsToken'],navigationExtras);
          break;
        case 'E':
            this.apiService.updateEmail(this.localDataService.user.id,form.email).subscribe(resp => {
              if(resp.status_response === 'SUCCESS'){
                this.localDataService.user.email = form.email;
                this.localDataService.saveUserData(this.localDataService.user);
              }
            });
          break;
      }
    }
  }

}
