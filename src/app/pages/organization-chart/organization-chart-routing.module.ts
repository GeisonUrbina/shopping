import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrganizationChartPage } from './organization-chart.page';

const routes: Routes = [
  {
    path: '',
    component: OrganizationChartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrganizationChartPageRoutingModule {}
