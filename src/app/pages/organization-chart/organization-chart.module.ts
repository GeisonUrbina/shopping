import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrganizationChartPageRoutingModule } from './organization-chart-routing.module';

import { OrganizationChartPage } from './organization-chart.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrganizationChartPageRoutingModule,
    ComponentsModule
  ],
  declarations: [OrganizationChartPage]
})
export class OrganizationChartPageModule {}
