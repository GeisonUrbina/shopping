import { Component, OnInit, Input } from '@angular/core';
import { ProductList } from '../../interfaces/interfaces';
import { NavParams, ModalController } from '@ionic/angular';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  product : ProductList = <ProductList>{};

  constructor(navParams: NavParams,private modalCtrl:ModalController, public localDataService:LocalDataService) {
    this.product = navParams.get('product');
  }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
