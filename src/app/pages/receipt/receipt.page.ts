import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LocalDataService } from '../../services/local-data.service';
import { Receipt } from '../../interfaces/interfaces';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.page.html',
  styleUrls: ['./receipt.page.scss'],
})
export class ReceiptPage implements OnInit {
  paymentType:string;
  isAnotherAmount:boolean;
  anotherAmount:Float32Array;
  receipt:Receipt = <Receipt>{};

  constructor(private apiService:ApiService,private localDataService:LocalDataService) {
    this.paymentType = "EF";
   }

  ngOnInit() {
    this.apiService.getReceipt(this.localDataService.cart,this.localDataService.user.id).subscribe(resp =>{
        this.receipt = resp;
    },error=>{
      console.log("Error:",error);
    });
  }

  setAnotherAmount(state:boolean){
    this.isAnotherAmount = state;
  }

}
