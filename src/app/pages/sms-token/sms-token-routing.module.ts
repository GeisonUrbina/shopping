import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SmsTokenPage } from './sms-token.page';

const routes: Routes = [
  {
    path: '',
    component: SmsTokenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SmsTokenPageRoutingModule {}
