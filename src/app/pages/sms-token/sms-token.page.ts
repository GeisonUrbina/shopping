import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { LocalDataService } from '../../services/local-data.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-sms-token',
  templateUrl: './sms-token.page.html',
  styleUrls: ['./sms-token.page.scss'],
})
export class SmsTokenPage implements OnInit {
  phone_code:string;
  data:any;
  status:string;
  cantidadIntentos:number = 3;
  mensajeIntentos:string = "";

  smsTokenForm = this.formBuilder.group(
    {
      code: ['',[Validators.required,Validators.pattern("^[0-9]{6}$")]]
    }
  );

  constructor(private router:Router,private apiService:ApiService,
    private formBuilder:FormBuilder,private localDataService:LocalDataService,private navCtrl:NavController) {
    if(this.router.getCurrentNavigation().extras.state){
      this.status = this.router.getCurrentNavigation().extras.state.status;
      this.data = this.router.getCurrentNavigation().extras.state.user;
      this.getSmsToken();
    }else{
      this.router.navigate(['/home']);
    }
    
   }

  ngOnInit() {
  }

  public submit(){
    if(this.smsTokenForm.value.code == this.phone_code){
      if(this.status === "CREADO"){
        this.localDataService.saveUserData(this.data);
        this.router.navigate(["/menu"]);
      }if(this.status === 'PHONE_MODIFY'){
        this.apiService.updatePhone(this.localDataService.user.id,this.data.temp_phone).subscribe(resp => {
          if(resp.status_response === 'SUCCESS'){
            this.localDataService.user.client_phone = this.data.temp_phone;
            this.localDataService.saveUserData(this.localDataService.user);
            this.navCtrl.navigateRoot('/configuration');
          }
        },error => {console.log(error)});
      }else{
        this.router.navigate(["/signing-up"]);
      }
    }else{
      this.mostrarMensajeIntentos();
    }
  }

  getSmsToken(){
    this.apiService.getSmsToken(this.data.client_phone).subscribe(resp => {
      this.phone_code = resp.token_sms;
    });
  }

  validateCode(value: any): void{
    this.router.navigate(["/home"]);
  }

  mostrarMensajeIntentos(){
    if(this.cantidadIntentos === 0){
      this.router.navigate(["/home"],{replaceUrl: true});
    }else{
      this.mensajeIntentos = "Usted tiene " + this.cantidadIntentos + " intento restante.";
      this.cantidadIntentos -= 1;
    }
  }
}
