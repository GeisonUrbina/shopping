import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SmsTokenPageRoutingModule } from './sms-token-routing.module';

import { SmsTokenPage } from './sms-token.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    SmsTokenPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SmsTokenPage]
})
export class SmsTokenPageModule {}
