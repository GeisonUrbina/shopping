import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SmsTokenPage } from './sms-token.page';

describe('SmsTokenPage', () => {
  let component: SmsTokenPage;
  let fixture: ComponentFixture<SmsTokenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsTokenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SmsTokenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
