import { Component, OnInit } from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  detail(order){
    let navigationExtras : NavigationExtras = {
      state:{
        order: order
      }
    }
    this.router.navigate(['shopping-detail'],navigationExtras);
  }

}
