import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { QuestionList } from '../../interfaces/interfaces';


@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {
  questionList: Observable<QuestionList[]>;
  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.questionList = this.dataService.getFaqPayment();
  }

}
