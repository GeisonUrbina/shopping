import { Component, OnInit } from '@angular/core';
import { IonItemSliding,IonItem } from '@ionic/angular';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.page.html',
  styleUrls: ['./favourite.page.scss'],
})
export class FavouritePage implements OnInit {
  itemSliding:IonItemSliding;
  slideOpts = {
    allowSlidePrev:false,
    allowSlideNext:false
  }
  constructor(public localDataService:LocalDataService) { }

  ngOnInit() {
  }

  public open(itemSlide: IonItemSliding, item: IonItem) {

  }

  public close(item: IonItemSliding) {

  }
}
