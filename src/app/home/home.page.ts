import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/interfaces';
import { MenuController,ToastController  } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { DataService } from '../services/data.service';
import { Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  phoneNumber:string;
  constructor(public menuCtrl:MenuController,private apiService:ApiService,
    private router:Router,private dataService:DataService,private toastCtrl:ToastController) {
    this.menuCtrl.enable(false,"mainMenu");
  }

  getLogin(phoneNumber:string,loginType:string){
    this.apiService.getLogin("84520006","T").subscribe(resp => {
      let navigationExtras : NavigationExtras = {
        state:{
          status: resp.status_response,
          user: resp.user
        }
      }

      this.presentToast(resp.status_response);
      this.router.navigate(['smsToken'],navigationExtras);
      
    }, error =>{
      this.presentToast(error.message);
    });
  }

  async presentToast(message) {
      const toast = await this.toastCtrl.create({
        message: message,
        duration: 2000
      });
      toast.present();
    }
  

}
