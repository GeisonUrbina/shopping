import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Favourite, User, Cart } from '../interfaces/interfaces';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class LocalDataService {

  cart: Cart[] = [];
  favourites: Favourite[] = [];
  user: User = <User>{};
  cartList:Cart = <Cart>{};
  constructor(private storage:Storage) { 
    this.uploadUser();
    this.uploadCartList();
    this.uploadFavouriteList();
  }

  saveToFavourites(clientId,product){
    const exist = this.favourites.find(fav => fav.producto.product_code === product.product_code);
    if(exist === undefined){
      this.favourites.push({"cliente":clientId,"producto":product});
      this.storage.set('favourites',this.favourites);
    }
  }

  saveToCart(clientId,productCode,quantity){
    const exist = this.cart.find(cart => cart.codigo === productCode);
    if(exist === undefined){
      this.cart.push({"cliente":clientId,"codigo":productCode,"cantidad":quantity});
      this.storage.set('cart',this.cart);
    }
  }

  isFavourite(clientId,productCode){
    const exist = this.favourites.find(fav => fav.producto.product_code === productCode && fav.cliente === clientId);
    return (exist === undefined)? false:true;
  }

  inCart(clientId,productCode){
    const exist = this.cart.find(cart => cart.codigo === productCode && cart.cliente === clientId);
    return (exist === undefined)? false:true;
  }
  
  modifyProductCartQuantity(clientId,productCode,quantity){
    for(let product of this.cart){
      if(product.cliente === clientId && product.codigo === productCode){
        product.cantidad = quantity;
        this.storage.set("cart",this.cart);
        break;
      }
    }
  }

  deleteProductInsideCart(clientId,productCode){
    for(let i = 0;i<this.cart.length;i++){
      if(this.cart[i].cliente === clientId && this.cart[i].codigo === productCode){
        this.cart.splice(i,1);
        this.storage.set("cart",this.cart);
      }
    }
  }

  deleteProductInsideFavourite(clientId,productCode){
    for(let i = 0;i<this.favourites.length;i++){
      if(this.favourites[i].cliente === clientId && this.favourites[i].producto.product_code === productCode){
        this.favourites.splice(i,1);
        this.storage.set("favourites",this.favourites);
      }
    }
  }

  saveUserData(user){
    this.user = user;
    this.storage.set('user',user);
  }

  async uploadFavouriteList(){
    const favourites = await this.storage.get('favourites');
    if(favourites) this.favourites = favourites;
  }

  async uploadCartList(){
    const cart = await this.storage.get('cart');
    if(cart) this.cart = cart;
  }

  async uploadUser(){
    this.user = await this.storage.get('user');
  }
}
