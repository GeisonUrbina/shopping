import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CategoryList, SubcategoryList, ProductList, Login, Receipt, Project } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  getLogin(phoneNumber:string,loginType:string){
    let usuario = new FormData();
    usuario.append('tipo_login',loginType);
    //if(loginType === "F") usuario.append('facebook_id',userData.id);
    if(loginType === "T") usuario.append('client_phone',phoneNumber);
    return this.http.post<Login>("https://www.urbidcr.com/app/validate/customer",usuario);
  }

  getCategories(){
    return this.http.get<CategoryList[]>(`https://www.urbidcr.com/app/category/categories`);
  }

  getSubcategories(categoryCode:string){
    return this.http.get<SubcategoryList[]>(`https://www.urbidcr.com/app/subcategory/subcategories/`+categoryCode);
  }

  getProducts(subcategoryCode:string){
    return this.http.get<ProductList[]>("https://www.urbidcr.com/app/product/products/" + subcategoryCode);
  }

  searchProducts(keyWord){
    let searchProduct = new FormData();
    searchProduct.append('keyWord',keyWord);
    return this.http.post<ProductList[]>("https://www.urbidcr.com/app/searchProduct",searchProduct);
  }

  getSmsToken(phoneNumber:string){
    let telefono = new FormData();
    telefono.append('number',phoneNumber);
    return this.http.post<any>("https://www.urbidcr.com/app/phoneCode",telefono);
  }

  getReceipt(cart,idClient){
    let receipt = new FormData();
    receipt.append('cart_products',JSON.stringify(cart));
    receipt.append('id_cliente',idClient);
    return this.http.post<Receipt>("https://www.urbidcr.com/app/receipt/receipt-list",receipt);
  }

  getCart(id_cliente,cart_products){
    let cartForm = new FormData();
    cartForm.append('cart_products',JSON.stringify(cart_products));
    cartForm.append('cliente',id_cliente);
    return this.http.post<ProductList[]>("https://www.urbidcr.com/app/cart/cart-products",cartForm);
  }

  getProjects(){
    return this.http.get<Project[]>("https://www.urbidcr.com/app/project/projects");
  }

  isAffiliated(clientId,projectCode){
    let enrollment = new FormData();
    enrollment.append('id_usuario',clientId);
    enrollment.append('project_code',projectCode);
    return this.http.post<any>("https://www.urbidcr.com/app/projects/isAffiliate",enrollment);
  }

  affiliate(clientId,projectCode){
    let affiliation = new FormData();
    affiliation.append('id_usuario',clientId);
    affiliation.append('project_code',projectCode);
    return this.http.post("https://www.urbidcr.com/app/projects/affiliations",affiliation);
  }

  disaffiliate(clientId){
    let disaffiliation = new FormData();
    disaffiliation.append('id_usuario',clientId);
    return this.http.post("https://www.urbidcr.com/app/projects/desaffiliation",disaffiliation);
  }

  updateName(clientId,name){
    let client = new FormData();
    client.append('user_name',name);
    client.append('id_usuario',clientId);
    return this.http.post<any>("https://www.urbidcr.com/app/modificar/nombre",client);
  }

  updateLastName(clientId,lastName){
    let client = new FormData();
    client.append('user_surname',lastName);
    client.append('id_usuario',clientId);
    return this.http.post<any>("https://www.urbidcr.com/app/modificar/apellido",client);
  }

  updatePhone(clientId,phone){
    let client = new FormData();
    client.append('user_phone',phone);
    client.append('id_usuario',clientId);
    return this.http.post<any>("https://www.urbidcr.com/app/modificar/telefono",client);
  }

  updateEmail(clientId,email){
    let client = new FormData();
    client.append('user_email',email);
    client.append('id_usuario',clientId);
    return this.http.post<any>("https://www.urbidcr.com/app/modificar/correo",client);
  }

}
