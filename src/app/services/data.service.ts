import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LinkList,QuestionList } from '../interfaces/interfaces';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient,private toastCtrl:ToastController) { }

  getMainMenuOptions(){
    return this.http.get<LinkList[]>('/assets/data/menu.json');
  }

  getSlideMenuOptions(){
    return this.http.get<LinkList[]>('/assets/data/slide-menu.json')
  }

  getFaqCommunity(){
    return this.http.get<QuestionList[]>('/assets/data/faq-community.json');
  }

  getFaqPayment(){
    return this.http.get<QuestionList[]>('/assets/data/faq-payment.json');
  }



  //General methods

  async presentToast(message:string,color:string) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 2000,
      color
    });
    toast.present();
  }

  addToFavourite(){
    this.presentToast("agregado a favoritos","danger");
  }

  addToCart(){
    this.presentToast("agregado al carrito","success");
  }

  genericMessage(message){
    this.presentToast(message,"dark");
  }
}
